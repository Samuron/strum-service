# frozen_string_literal: true

# rubocop: disable Style/SymbolArray
RSpec.describe "instance evaluated block" do
  def call_service(service_class, input)
    service_class.call(input) do
      success { _1 }
      failure { _1 }
    end
  end

  it "success" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        output(1)
      end
    end

    expect(call_service(service_class, {})).to eq(1)
  end

  it "error" do
    service_class = Class.new do
      include Strum::Service
      define_method(:call) do
        add_error(:b, :err1)
        add_error(:b, :err2)
      end
    end

    expect(call_service(service_class, {})).to eq({ b: [:err1, :err2] })
  end
end
# rubocop: enable Style/SymbolArray
