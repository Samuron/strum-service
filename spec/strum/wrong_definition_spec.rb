# frozen_string_literal: true

RSpec.describe Strum::Service::Failure do
  subject(:wrong_class) do
    Class.new do
      include Strum::Service
    end
  end

  context "without call method" do
    it { expect { wrong_class.call({}) }.to raise_exception(described_class) }
  end
end
